//
//  ViewController.h
//  SimplePlistReader
//
//  Created by User on 4/23/15.
//  Copyright (c) 2015 steven. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)convertPlistToCSV:(id)sender;
- (IBAction)convertCSVToPlist:(id)sender;

@end

