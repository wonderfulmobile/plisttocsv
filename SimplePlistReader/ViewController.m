//
//  ViewController.m
//  SimplePlistReader
//
//  Created by User on 4/23/15.
//  Copyright (c) 2015 steven. All rights reserved.
//

#import "ViewController.h"
#import "CSVParser.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)convertPlistToCSV:(id)sender {
    [self readDataFromPlist];
}

- (IBAction)convertCSVToPlist:(id)sender {
    [self csvParser];
}

#pragma mark - read csv & write data to plist
- (void) csvParser
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *root = [documentsDir stringByAppendingPathComponent:@"result_04.plist"];
    
    NSString *sourceFile = [[NSBundle mainBundle]
                      pathForResource:@"01" ofType:@"csv"];
    
    CSVParser* parser = [[CSVParser alloc] init];
    NSMutableArray* m_arrayData = [parser parseCSV:sourceFile];
    
    NSMutableArray * dataArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < [m_arrayData count]; i ++) {
        NSArray* __aray = [m_arrayData objectAtIndex:i];
        
        NSDictionary* dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[__aray objectAtIndex:0], @"bundleName", [__aray objectAtIndex:1], @"appName", [__aray objectAtIndex:2], @"apiKey", [__aray objectAtIndex:3], @"placementKey", @"1.0.3", @"boundleVersion", nil];
        
        [dataArray addObject:dictionary];
    }
    
    BOOL isSuccessed = [dataArray writeToFile:root atomically:YES];
    
    isSuccessed = isSuccessed;
    
    if (isSuccessed) {
        NSLog(@" file path ---- \n%@", root);
    }
    
    m_arrayData = nil;
}

#pragma mark - read plist & write data to csv
- (void) readDataFromPlist
{
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"05" ofType:@"plist"]];
    
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    //NSString *documentsDir = [paths objectAtIndex:0];
    //NSString *root = [documentsDir stringByAppendingPathComponent:@"result_01.plist"];
    
    //NSArray* array = [[NSArray alloc] initWithContentsOfFile:root];
    
    [self writeDataToCVS:dic];
}

- (void) writeDataToCVS:(NSMutableDictionary*)response
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *root = [documentsDir stringByAppendingPathComponent:@"05.csv"];
    
    NSString *temp = @"";
    
    for( NSString *aKey in [response allKeys] )
    {
        if (![aKey isEqualToString:@"User"]) {
            continue;
        }
        NSDictionary *response1 = [response objectForKey:aKey];
        for( NSString *bKey in [response1 allKeys] )
        {
            NSDictionary *response2 = [response1 objectForKey:bKey];
            for( NSString *cKey in [response2 allKeys] )
            {
                if ([cKey isEqualToString:@"CFBundleIdentifier"]) {
                    
                    temp = [temp stringByAppendingFormat:@"\n %@", [response2 valueForKey:cKey]];
                }
                if ([cKey isEqualToString:@"CFBundleDisplayName"]) {
                    
                    temp = [temp stringByAppendingFormat:@", %@", [response2 valueForKey:cKey]];
                }
            }
        }
    }
    
    NSError* error = nil;
    BOOL isSuccessed = [temp writeToFile:root atomically:NO encoding:NSUTF8StringEncoding error:&error];
    
    isSuccessed = isSuccessed;
    
    if (isSuccessed) {
        NSLog(@" file path ---- \n%@", root);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
